import json

with open("./public/static/member_pages_OLD.json", "r") as src_handle, open("./public/static/member_pages.json", "w") as write_handle:

    new_data = []

    data = json.loads(src_handle.read())

    for d in data:
        #new_data[d]["links"] = [{"title":l[1], "url": l[0]} for l in links]
        new_data.append({
            "url": d[0],
            "title": d[1]
        })
    write_handle.write(json.dumps(new_data, indent=3))