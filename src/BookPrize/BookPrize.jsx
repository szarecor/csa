import HtmlPageLoader from '../HtmlPageLoader/HtmlPageLoader';

class BookPrize extends HtmlPageLoader {
    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Book Prize",
            htmlFile: "book_prize.htm",
            pageContent: null
        };
    }
}

export default BookPrize;