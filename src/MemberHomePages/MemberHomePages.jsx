import React from 'react';
import withAsyncJsonData from '../Utils/Utils';

function BaseMemberLinks(incomingData) {

    document.title = "Member Pages - Czechoslovak Studies Association"
    const data = incomingData.data;

    if (data === null) return <div>Loading...</div>

    return <>
        <h2>CSA Members Webpages</h2>
        <ul>{ data.map((x, i) => {
            return (
                <li key={i}>
                    <a href={x.url} target="_blank" rel="noopener noreferrer">{x.title}</a>
                </li>
            );
        })}
        </ul> 
    </>;
}

const MemberLinks = withAsyncJsonData(BaseMemberLinks, "member_pages.json");
export default MemberLinks;

/*
import React from 'react';

class MemberHomePages extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Member Pages - Czechoslovak Studies Association"
            , pages: [
               [ "http://www.la.wayne.edu/polisci/kdk", "Kevin Deegan-Krause"]
               , ["http://www.history.ubc.ca/people/eagle-glassheim", "Eagle Glassheim"]
               , ["http://www.indiana.edu/~histweb/faculty/Display.php?Faculty_ID=67", "Owen Johnson"]
               , ["http://chnm.gmu.edu/history/faculty/kelly/", "Mills Kelly"]
               , ["http://www.centraleuropeanobserver.com/", "Daniel Miller"]
               , ["http://history.pages.tcnj.edu/faculty/cynthia-j-paces/", "Cynthia Paces"]
               , ["https://www.public.iastate.edu/~zarecor/#", "Kimberly Zarecor"]
           ]
        }
    }

    componentDidMount() {
        const {pageTitle} = this.state;
        document.title = pageTitle;
    }

    render() {
        const {pages} = this.state;

        return (
        <div>
            <h3>CSA Member Pages</h3>
            <p>(Good resources on Czech, Slovak, and East Central European history)</p>

            <ul>
            {
            pages.map(page => {
                return <li><a rel="noopener noreferrer" href={page[0]} target="_blank">{page[1]}</a></li>
            })
            }
            </ul>
            <p>Please email <a href="mailto:zarecor@iastate.edu">Kimberly Zarecor</a> to add a link to your home page.</p>
        </div>

        );

    }
}


export default MemberHomePages;



*/