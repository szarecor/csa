import HtmlPageLoader from '../HtmlPageLoader/HtmlPageLoader';

class Bylaws extends HtmlPageLoader {
    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Bylaws",
            htmlFile: "bylaws.htm",
            pageContent: null
        };
    }
}

export default Bylaws;