import React from 'react';
import { Link } from "react-router-dom";
import './Navigations.css';

class Navigation extends React.Component {

    componentDidMount() {

        this.toggleSubNav = this.toggleSubNav.bind(this);
        this.forceSubnavClose = this.forceSubnavClose.bind(this);
    }


    forceSubnavClose(e) {
        /* The nav needs to close when navigating or else the UX is weird, this does that */
        
        //this.toggleSubNav(e);        
        let el = e.target;
        while (!el.classList.contains("submenu-root") && el.tagName !== "NAV") el = el.parentElement;

        el.classList.add("force-close");

        
        window.setTimeout(
            () => el.classList.remove("force-close")
            , 200
        );
        
    }

    toggleSubNav(e) {
        // Climb the dom tree to find the correct dom el:
        let targ = e.target;
        while (!targ.classList.contains("submenu-root")) targ = targ.parentElement;
        targ.classList.toggle("hover", e.type === "mouseover" || e.type === "touchstart");
    }

    render() {

        const closerFunc = (e) => {
            this.toggleSubNav(e);
        }

        return (
        <nav>
            <ul>
                <li><Link to="/">Home</Link></li>
                <li onTouchStart={this.toggleSubNav} onTouchCancel={this.toggleSubNav} onMouseOver={this.toggleSubNav} onMouseOut={this.toggleSubNav} className="submenu-root"><span>About</span>
                    <ul>
                        <li><Link onClick={closerFunc} to="/bylaws">Bylaws</Link></li>
                        <li><Link onClick={closerFunc} to="/officers">Officers</Link></li>
                        <li><Link onClick={closerFunc} to="/membership">Membership</Link></li>
                        <li><Link onClick={closerFunc} to="/csa-history">CSA history
                            <br /><small>(in Czech)</small></Link>
                        </li>
                    </ul>
                </li>

                <li><Link to="/pech-prize">Pech Prize</Link></li>
                <li><Link to="/book-prize">Book Prize</Link></li>

                <li onTouchCancel={this.toggleSubNav} onTouchStart={this.toggleSubNav} onMouseOver={this.toggleSubNav} onMouseOut={this.toggleSubNav} className="submenu-root" id="nav_resources"><span>Resources</span>
                    <ul>
                        <li><Link onClick={closerFunc} to="/list-serve">List-Serve</Link></li>
                        <li><a href="http://listserv.gmu.edu/cgi-bin/wa?A0=CHC-LIST-L">List-Serve Archive</a></li>
                        <li><Link onClick={closerFunc} to="/newsletter">Newsletters</Link></li>
                        <li><a href="http://new-york.czechcentres.cz/program/event-details/czech-studies-workshop1/">Czech Studies Workshop 2013</a></li>
                        <li><a href="http://www.utexas.edu/cola/centers/creees/symposia/Czech-Studies-Workshop.php">Czech Studies Workshop 2012</a></li>
                        <li><Link onClick={closerFunc} to="/czech-studies-workshop-2011">Czech Studies Workshop 2011</Link></li>
                        <li><Link onClick={closerFunc} to="/translated-documents">Translated Documents</Link></li>
                        <li><Link onClick={closerFunc} to="/links">Links</Link></li>
                        <li><Link onClick={closerFunc} to="/related-organizations">Related Organizations</Link></li>
                        <li><Link onClick={closerFunc} to="/member-pages">Member Homepages</Link></li>
                    </ul>
                </li>
            </ul>
        </nav>
        );
    }
}


export default Navigation;



