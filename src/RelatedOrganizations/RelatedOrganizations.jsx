import React from 'react';
import withAsyncJsonData from '../Utils/Utils';

const BaseRelatedOrganizationsPage = (incomingData) => {

    document.title = "Related Organizations - Czechoslovak Studies Association"
    const orgs = incomingData.data;

    if (orgs === null) return <div>Loading...</div>

    return (
        <div>
            <h2>Related Organizations</h2>
            <ul>
                {
                    orgs.map((link, i) => {
                        return <li key={i}><a href={link.url} target="_blank" rel="noopener noreferrer">{link.title}</a></li>
                    })
                }
            </ul>
        </div>

    );

};

const RelatedOrganizationsPage = withAsyncJsonData(BaseRelatedOrganizationsPage, "related_organizations.json");
export default RelatedOrganizationsPage;