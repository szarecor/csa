import HtmlPageLoader from '../HtmlPageLoader/HtmlPageLoader';

class Officers extends HtmlPageLoader {

    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Officers",
            htmlFile: "officers.htm",
            pageContent: null
        };
    }
}

export default Officers;