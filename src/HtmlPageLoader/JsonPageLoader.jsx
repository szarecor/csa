
import React from 'react';
import Loading from '../Loading/Loading';

class JsonPageLoader extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            pageTitle: `${props.pageTitle} - Czechoslovak Studies Association`
            , jsonFile: props.jsonFile
            , pageData: null //sessionStorage.getItem(storageKey)
        }
    }

    componentDidMount() {
        console.log("MOUNTING")

        const {pageTitle, jsonFile} = this.state   
            , path = `${process.env.REACT_APP_BASEPATH}/static/${jsonFile}`;

        sessionStorage.removeItem(jsonFile);

        document.title = `${pageTitle} - Czechoslovak Studies Association`;

        let pageData = sessionStorage.getItem(jsonFile);

        if (!pageData) {
            fetch(path)
                .then((resp) => resp.text())
                .then((jsonString) => {
                    console.log("WHAT IS JSON STRING?");
                    console.log(jsonString)
                    sessionStorage.setItem(jsonFile, jsonString);

                    this.setState({
                        pageData: JSON.parse(jsonString)
                    });
                });
        } else {

            this.setState({
                pageData: pageData
            });
        }
    }

    render() {
        const {pageData} = this.state;

        if (pageData === null) {
            return <Loading />
        } else {
            return (<React.Fragment>
                {JSON.stringify(pageData)}
            </React.Fragment>); 
        }
    }
}

export default JsonPageLoader;