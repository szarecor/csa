import React from 'react';
import Loading from '../Loading/Loading';

class HtmlPageLoader extends React.Component {

    constructor(props) {
        super(props);


        this.state = {
            pageTitle: `${props.pageTitle} - Czechoslovak Studies Association`
            , htmlFile: props.htmlFile
            , pageContent: null //sessionStorage.getItem(props.pageTitle)
        }

        this.myRef = React.createRef();
    }

    componentDidUpdate() {
        /** This is necessary to have <a> tags in the html strings imported via the fetch() api
         * converted into something that uses React's Link system instead of native browser behavior
         */
        let anchs = this.myRef.current.querySelectorAll("a");
        anchs.forEach(a => {
            const href = a.href
                , url = new URL(href);
            if (url.host !== window.location.host) return;
            a.removeAttribute("href");
            a.onclick = () => this.props.history.push(url.pathname);
        })
    }

    componentDidMount() {

        const {pageTitle, htmlFile} = this.state   
            , basepath = process.env.REACT_APP_BASEPATH 
            , path = `${basepath}/static/${htmlFile}`;

        // For dev purposes only:
        //sessionStorage.removeItem(htmlFile);

        document.title = `${pageTitle} - Czechoslovak Studies Association`;

        let pageContent = null; //sessionStorage.getItem(htmlFile);

        if (!pageContent) {

            fetch(path)
                .then((resp) => resp.text())
                .then((html) => {
                    //sessionStorage.setItem(htmlFile, html);
                    this.setState({
                        pageContent: html
                    });
                });
        } else {

            this.setState({
                pageContent: pageContent
            });
        }
    }

    render() {
        const {pageContent} = this.state;

        if (pageContent === null) {
            return <Loading />
        } else {
            return <div ref={this.myRef} dangerouslySetInnerHTML={{__html: pageContent}} />; 
        }
    }
}

export default HtmlPageLoader;