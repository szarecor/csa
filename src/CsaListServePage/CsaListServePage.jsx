import HtmlPageLoader from '../HtmlPageLoader/HtmlPageLoader';

class CsaListServePage extends HtmlPageLoader {
    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Listserve Information",
            htmlFile: "csa_listserve_page.htm",
            pageContent: null
        };
    }
}

export default CsaListServePage;