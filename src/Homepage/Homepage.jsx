import React from 'react';
import {NavLink} from "react-router-dom";

class Homepage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Czechoslovak Studies Association"
        }
    }

    componentDidMount() {
        /** This handles redirects from a 404 page when the server cannot recognize a client-side path
         * It's here and not in App.js or somewhere else that might make more sense because it NEEDS 
         * to be inside the React.Router wrapper
         */
        if (window.location.search) {
            const basepath = process.env.REACT_APP_BASEPATH 
                , pth = window.location.search.replace("?p=", "").replace(basepath, "");
            this.props.history.push(pth);
        }

        const {pageTitle} = this.state;
        document.title = pageTitle;
    }

    render() {
        return (
            <div>
                <p>The Czechoslovak Studies Association (CSA) unites over a hundred scholars, both in the United States and abroad, who share an interest in the history and culture of Czechoslovakia, its predecessor and successor states, and all its peoples, within and without its historical boundaries. Since its first meeting in 1974, the CSA has worked to encourage scholarly work in Czechoslovak history and related fields, to provide a network and information resources for Czech and Slovak studies, and to collaborate with scholars and institutions having similar aims.</p>

                <p>To that end, the CSA:</p>
                <ul>
                    <li>Provides connections between students and scholars interested in its subject</li>
                    <li>Supports related teaching, research, and publication, notably by awarding the Stanley Pech Prize and the Czechoslovak Studies Association Book Prize</li>
                    <li>Publishes the Czechoslovak Studies Newsletter (two numbers per year)</li>
                    <li>Maintains this website</li>

                </ul>

                <h3>Latest News</h3>
                <ul id="news_list">
                    <li><NavLink to="/pech-prize">2018 Pech Prize Winner Announced</NavLink></li>
                    <li><NavLink to="/book-prize">2017 Book Prize Winners Announced</NavLink></li>
                </ul>
                <p>The CSA website is constantly evolving. We welcome your contribution of new links or those that we've missed that will be of interest to the Czech and Slovak studies community. In particular, we encourage members and visitors to  contribute documents on Czech and Slovak history, culture, and public affairs either in the original or in translation.</p>

                <p>In 1990, the CSA was incorporated in the state of Ohio as a not-for-profit  corporation. It is an affiliated society of the <a rel="noopener noreferrer" target="_blank" href="http://www.historians.org">American Historical Association</a> (AHA) and the <a target="_blank" rel="noopener noreferrer" href="http://www.aseees.org"> Association for Slavic, East European and Eurasian Studies</a> (ASEEES). The CSA convenes yearly at the ASEEES annual meeting. We also have informal affiliations with the <a rel="noopener noreferrer" target="_blank" href="http://www.cas.umn.edu/">Center for Austrian Studies</a>, the <a href="http://www.slovakstudies.org/">Slovak Studies Association</a>, the <a href="http://cas.umn.edu/links/related.html">Society for Austrian and Habsburg History</a>, and the H-Net list <a target="_blank" rel="noopener noreferrer" href="http://www2.h-net.msu.edu/~habsweb/">HABSBURG</a>.</p>

                <p>Please email <a href="mailto:zarecor@iastate.edu">Kimberly Zarecor</a> with any comments or additions.</p>
            </div>
        );
    }
}

export default Homepage;