import React from 'react';

class Membership extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Membership - Czechoslovak Studies Association"
        }
    }

    componentDidMount() {
        const {pageTitle} = this.state;
        document.title = pageTitle;
    }

    render() {
        return (
            <React.Fragment><h2 class="">Coming Soon</h2></React.Fragment>
        );
    }
}

export default Membership;