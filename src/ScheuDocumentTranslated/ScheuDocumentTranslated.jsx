import HtmlPageLoader from '../HtmlPageLoader/HtmlPageLoader';

class ScheuDocumentTranslated extends HtmlPageLoader {

    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Robert Scheu Translated",
            htmlFile: "scheu_document_translated.htm",
            pageContent: null
        };
    }
}

export default ScheuDocumentTranslated;