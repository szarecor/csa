import React from 'react';

function withAsyncJsonData(PassedComponent, jsonFile) {
    /** Takes a component and returns that component wrapped by
     * a component that provides async json loading functionality
     * to the wrapped component
     */

    return class JsonLoaderComponent extends React.Component {

        constructor(props) {
            super();

            this.state = {
                jsonFile: jsonFile,
                data: null
            };
        }

        render() {
            const {data} = this.state;
            return (<React.Fragment><PassedComponent data={data} /></React.Fragment>);
        }

        componentDidMount() {
            const {jsonFile} = this.state
                , __this = this
                , basepath = process.env.REACT_APP_BASEPATH 
                , path = `${basepath}/static/${jsonFile}`;

            fetch(path)
                .then(resp => resp.json())
                .then(data => {
                    __this.setState({
                        data: data 
                    });
                });
        }
    }
}

export default withAsyncJsonData