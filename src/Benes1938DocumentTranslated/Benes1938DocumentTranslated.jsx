import HtmlPageLoader from '../HtmlPageLoader/HtmlPageLoader';

class Benes1938DocumentTranslated extends HtmlPageLoader {

    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Benes 1938 Translated",
            htmlFile: "benes_1938_document.htm",
            pageContent: null
        };
    }
}

export default Benes1938DocumentTranslated;