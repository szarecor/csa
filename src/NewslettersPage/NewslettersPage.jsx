import HtmlPageLoader from '../HtmlPageLoader/HtmlPageLoader';

class NewslettersPage extends HtmlPageLoader {

    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Newsletters",
            htmlFile: "newsletters_page.htm",
            pageContent: null
        };
    }
}

export default NewslettersPage;