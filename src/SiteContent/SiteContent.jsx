
import React, { Component, Suspense } from 'react';
import { BrowserRouter, Route } from "react-router-dom";
import SiteHeader from "./SiteHeader/SiteHeader";
import Homepage from "./Homepage/Homepage";
import PechPrizeWrapper from './PechPrize/PechPrizeWrapper';

const BookPrize = React.lazy(() => import('./BookPrize/BookPrize'));
const Officers = React.lazy(() => import('./Officers/Officers'));
const Bylaws = React.lazy(() => import("./Bylaws/Bylaws"));
const LinksPage = React.lazy(() => import("./LinksPage/LinksPage"));
const RelatedOrganizations = React.lazy(() => import("./RelatedOrganizations/RelatedOrganizations"));
const CzechStudiesWorkshop2011 = React.lazy(() => import("./CzechStudiesWorkshop2011/CzechStudiesWorkshop2011"));
const CsaHistory = React.lazy(() => import("./CsaHistory/CsaHistory"));
const MemberHomePages = React.lazy(() => import("./MemberHomePages/MemberHomePages"));
const CsaListServePage = React.lazy(() => import("./CsaListServePage/CsaListServePage"));
const TranslatedDocumentsPage = React.lazy(() => import("./TranslatedDocumentsPage/TranslatedDocumentsPage"));
const NewslettersPage = React.lazy(() => import("./NewslettersPage/NewslettersPage"));
const ScheuDocumentTranslated = React.lazy(() => import("./ScheuDocumentTranslated/ScheuDocumentTranslated"));
const Benes1938DocumentTranslated = React.lazy(() => import("./Benes1938DocumentTranslated/Benes1938DocumentTranslated"));
//const PechPrize = React.lazy(() => import("./PechPrize/PechPrizeWrapper"));

class SiteContent extends Component {
  render() {

    return (
    <Suspense fallback={<div>LOADING</div>}>
        <BrowserRouter basename="/csa">
        <div>
            <Route path="/" exact component={Homepage} />
            <Route path="/officers" component={Officers} />
            <Route path="/bylaws" component={Bylaws} />
            <Route path="/pech-prize" component={PechPrizeWrapper} />
            <Route path="/book-prize" component={BookPrize} />
            <Route path="/links" component={LinksPage} />
            <Route path="/newsletter" component={NewslettersPage} />
            <Route path="/related-organizations" component={RelatedOrganizations} />
            <Route path="/czech-studies-workshop-2011" component={CzechStudiesWorkshop2011} />
            <Route path="/csa-history" component={CsaHistory} />
            <Route path="/member-pages" component={MemberHomePages} />
            <Route path="/list-serve" component={CsaListServePage} />
            <Route path="/translated-documents" exact component={TranslatedDocumentsPage} />
            <Route path="/documents/scheu" component={ScheuDocumentTranslated} />
            <Route path="/documents/benes-1938" component={Benes1938DocumentTranslated} />
        </div>
</BrowserRouter>
</Suspense>
    );
  }
}

export default SiteContent;
