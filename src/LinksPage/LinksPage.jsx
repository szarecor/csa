import React from 'react';
import withAsyncJsonData from '../Utils/Utils';

function BaseLinksPage(incomingData) {

    document.title = "Links - Czechoslovak Studies Association"
    const data = incomingData.data;

    if (data === null) return <div>Loading...</div>

    return <>
        <h2>Links</h2>
        <div>{ Object.keys(data).map(k => {
            const linksGroup = data[k];
            return (
            <section key={k}>
                <h3 key={k}>{linksGroup.title}</h3>
                <ul>
                {linksGroup.links.map((l, i) => {
                    const targ = l["url"].indexOf("http") === 0 ? "_blank" : "_self";

                    return (
                    <li key={i}>
                        <a target={targ} href={l["url"]}>{l["title"]}</a>
                    </li>
                    );
                })}
                </ul>
            </section>
            );
        })}
        </div> 
    </>;
}

const LinksPage = withAsyncJsonData(BaseLinksPage, "links.json");
export default LinksPage;