import HtmlPageLoader from '../HtmlPageLoader/HtmlPageLoader';

class PechPrize extends HtmlPageLoader {

    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Pech Prize",
            htmlFile: "pech_prize.htm",
            pageContent: null
        };
    }

}

export default PechPrize;