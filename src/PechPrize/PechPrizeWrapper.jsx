import React, { Suspense } from 'react';

//const PechPrize = React.lazy(() => import("./PechPrize"));

const PechPrizeWrapper = (props) => {
    return (
        <Suspense fallback={<div>LOADING</div>}>
        { props.children }
        </Suspense>

    )
}

export default PechPrizeWrapper;