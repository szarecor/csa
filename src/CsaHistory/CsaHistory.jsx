import HtmlPageLoader from '../HtmlPageLoader/HtmlPageLoader';

class CsaHistoryPage extends HtmlPageLoader {
    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "History of the CSA",
            htmlFile: "csa_history.htm",
            pageContent: null
        };
    }
}

export default CsaHistoryPage;