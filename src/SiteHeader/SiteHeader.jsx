import React from 'react';
import './SiteHeader.css';

const SiteHeader = (props) => {
    const logoImage = require("../images/logo.jpg");

    return (
        <header>
            <div id="header_main_column">
                <img src={logoImage} alt="Czechoslovak Studies Association logo" title="Czechoslovak Studies Association" />
            </div>
        </header>
    );
}

export default SiteHeader;