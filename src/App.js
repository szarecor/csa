import './App.css'
import React, { Component } from 'react';
import { BrowserRouter, Route } from "react-router-dom";
import SiteHeader from "./SiteHeader/SiteHeader";
import Homepage from "./Homepage/Homepage";
import Officers from './Officers/Officers';
import Bylaws from './Bylaws/Bylaws';
import LinksPage from './LinksPage/LinksPage';
import RelatedOrganizations from './RelatedOrganizations/RelatedOrganizations';
import Navigation from './Navigation/Navigation';
import PechPrize from './PechPrize/PechPrize';
import BookPrize from './BookPrize/BookPrize';
import CsaHistoryPage from './CsaHistory/CsaHistory';
import CzechStudiesWorkshop2011Page from './CzechStudiesWorkshop2011/CzechStudiesWorkshop2011';
import MemberLinks from "./MemberHomePages/MemberHomePages";
import CsaListServePage from "./CsaListServePage/CsaListServePage";
import TranslatedDocumentsPage from "./TranslatedDocumentsPage/TranslatedDocumentsPage";
import NewslettersPage from "./NewslettersPage/NewslettersPage";
import ScheuDocumentTranslated from "./ScheuDocumentTranslated/ScheuDocumentTranslated";
import Benes1938DocumentTranslated from "./Benes1938DocumentTranslated/Benes1938DocumentTranslated";
import Membership from "./Membership/Membership";
/*
const PechPrize = React.lazy(() => import('./PechPrize/PechPrize'));
const BookPrize = React.lazy(() => import('./BookPrize/BookPrize'));
const Officers = React.lazy(() => import('./Officers/Officers'));
const Bylaws = React.lazy(() => import("./Bylaws/Bylaws"));
const LinksPage = React.lazy(() => import("./LinksPage/LinksPage"));
const RelatedOrganizations = React.lazy(() => import("./RelatedOrganizations/RelatedOrganizations"));
const CzechStudiesWorkshop2011 = React.lazy(() => import("./CzechStudiesWorkshop2011/CzechStudiesWorkshop2011"));
const CsaHistory = React.lazy(() => import("./CsaHistory/CsaHistory"));
const MemberHomePages = React.lazy(() => import("./MemberHomePages/MemberHomePages"));
const CsaListServePage = React.lazy(() => import("./CsaListServePage/CsaListServePage"));
const TranslatedDocumentsPage = React.lazy(() => import("./TranslatedDocumentsPage/TranslatedDocumentsPage"));
const NewslettersPage = React.lazy(() => import("./NewslettersPage/NewslettersPage"));
const ScheuDocumentTranslated = React.lazy(() => import("./ScheuDocumentTranslated/ScheuDocumentTranslated"));
const Benes1938DocumentTranslated = React.lazy(() => import("./Benes1938DocumentTranslated/Benes1938DocumentTranslated"));
const PechPrize = React.lazy(() => import("./PechPrize/PechPrize"));
*/

class App extends Component {
  /*
  componentDidMount() {
    console.log(window.location.search);
    if (window.location.search) {
      const pth = window.location.search.replace("?p=", "")
      this.props.history.push(pth);
    }
  }
  */

  render() {
    const basepath = process.env.REACT_APP_BASEPATH 

    return (

        <div className="App">
            <div id="main_wrapper">
                <div id="left_column"></div>

                <div id="main_column">
                        <div>
                          <SiteHeader basepath={basepath} />
                        <BrowserRouter basename={basepath}>
                          <React.Fragment> 
                            <Navigation basepath={basepath} />


                        <div id="main_column_inner">
                            <Route path="/" exact component={Homepage} />
                            <Route path="/officers" component={Officers} />
                            <Route path="/bylaws" component={Bylaws} />
                            <Route path="/membership" component={Membership} />
                            <Route path="/pech-prize" component={PechPrize} />
                            <Route path="/book-prize" component={BookPrize} />
                            <Route path="/links" render={()=> <LinksPage pageTitle="Links" jsonFile="links.json" basepath={basepath} />} />
                            <Route path="/newsletter" component={NewslettersPage} />
                            <Route path="/related-organizations" component={RelatedOrganizations} />
                            <Route path="/czech-studies-workshop-2011" component={CzechStudiesWorkshop2011Page} />
                            <Route path="/csa-history" component={CsaHistoryPage} />
                            <Route path="/member-pages" component={MemberLinks} />
                            <Route path="/list-serve" component={CsaListServePage} />
                            <Route path="/translated-documents" exact component={TranslatedDocumentsPage} />
                            <Route path="/documents/scheu" component={ScheuDocumentTranslated} />
                            <Route path="/documents/benes-1938" component={Benes1938DocumentTranslated} />
                        </div>
                        </React.Fragment>
                </BrowserRouter>
                        </div>
                </div>
                <div id="right_column"></div>

            </div>

      </div>
    );
  }
}

export default App;
 