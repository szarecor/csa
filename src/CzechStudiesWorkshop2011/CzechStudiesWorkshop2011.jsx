import HtmlPageLoader from '../HtmlPageLoader/HtmlPageLoader';

class CzechStudiesWorkshop2011Page extends HtmlPageLoader {
    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Czech Studies Workshop 2011",
            htmlFile: "czech_studies_workshop_2011.htm",
            pageContent: null
        };
    }
}

export default CzechStudiesWorkshop2011Page;