import HtmlPageLoader from '../HtmlPageLoader/HtmlPageLoader';

class TranslatedDocumentsPage extends HtmlPageLoader {
    constructor(props) {
        super(props);

        this.state = {
            pageTitle: "Translated Documents",
            htmlFile: "translated_documents.htm",
            pageContent: null
        };
    }
}

export default TranslatedDocumentsPage;